"""
Python module containing plotting utilities

Copyright 2016, 2017, 2018 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""

# Import plotting library and math functions
from matplotlib import rc
from matplotlib.pyplot import plot, savefig, show, clf
from numpy import polyval
import numpy as np
import matplotlib as mpl


linesize = 2
ticksize = 20
rc('lines', lw=linesize, mew=linesize)
rc('xtick', labelsize=ticksize)
rc('ytick', labelsize=ticksize)

# ---------------------------- Functions -----------------------------


def plot_data_fit_and_errors(xdata, ydata, fit_coeffs, fit_errors):
    """Plots data, fit and uncertainty bounds on one graph in
    consistent colour / line style"""
    plot(xdata, ydata, '-x')
    plot(xdata, polyval(fit_coeffs, xdata), 'r')
    plot(xdata, polyval(fit_coeffs + fit_errors, xdata), 'r--')
    plot(xdata, polyval(fit_coeffs - fit_errors, xdata), 'r--')


def my_savefig(filename):
    'Save figure with border removed'
    savefig(filename, bbox_inches='tight')


def show_or_clear(show_fits):
    'Display current plot if required'
    if show_fits:
        show()
    else:
        clf()


def shift_colormap(cmap, data, name=None):
    '''
    Adapted from: https://stackoverflow.com/questions/7404116/
    defining-the-midpoint-of-a-colormap-in-matplotlib#20528097
    ---------------------------------------------------------
    Function to offset the "center" of a colormap. Useful for
    "normalising" a diverging colormap such that the center
    is on zero, the diverging strands are of the scale and
    the dynamic range fits the data.
    ---------------------------------------------------------
    Input
    -----
      cmap : The matplotlib colormap to be altered
      data : The (array-like) data to which the colormap is to be
                 normalised
    '''
    # Work out start, midpoint and end based on the data
    data_max = np.max(data)
    data_min = np.min(data)
    extrema_max = np.max((abs(data_max), abs(data_min)))
    start = 0.0
    midpoint = 0.5
    stop = 1.0
    if extrema_max == abs(data_max):  # +ve data range > -ve data range
        if data_min > 0:  # All data +ve
            # midpoint + fraction into second half at which data starts
            start = 0.5 * float(data_min) / extrema_max + 0.5
        elif data_min < 0:  # Some data +ve, some data -ve
            # midpoint - fraction back into first half at which data starts
            start = 0.5 * (1 - float(abs(data_min)) / extrema_max)
        else:  # data_min == 0
            start = 0.5  # start from midpoint
    elif extrema_max == abs(data_min):  # -ve data range > +ve data range
        if data_max < 0:  # All data -ve
            # midpoint - fraction back into first half at which data stops
            stop = 0.5 * (1 - float(abs(data_max)) / extrema_max)
        elif data_max > 0:  # Some data +ve, some data -ve
            # midpoint + fraction into second half at which data stops
            stop = 0.5 * float(data_max) / extrema_max + 0.5
        else:  # data_max == 0
            stop = 0.5  # stop at midpoint
    # Initialise cdict
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }
    # Regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)
    # Shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])
    # Loop to define cdict
    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)
        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))
    # Return
    if name is None:
        name = cmap.name + '_shifted'
    newcmap = mpl.colors.LinearSegmentedColormap(name, cdict)
    return newcmap


def shift_axis_for_pcolormesh(x):
    dx = []
    for i in range(len(x) - 1):
        dx.append(x[i + 1] - x[i])
    dx = np.array(dx)
    dx = np.concatenate([np.array([dx[0]]), dx, np.array([dx[-1]])])
    x_shift = np.zeros_like(dx)
    for i in range(len(dx) - 1):
        x_shift[i] = x[i] - dx[i] / 2.0
    x_shift[-1] = x[-1] + dx[-1] / 2.0
    return x_shift

# ------------------------ End Functions -----------------------------
