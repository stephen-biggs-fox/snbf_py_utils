"""
Class to animate contour plots to represent 3D data

Copyright 2017, 2018 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""


from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np


class ContourPlotAnimator():

    def __init__(self, x_axis, y_axis, z_axis, data, xlabel='x', ylabel='y',
                 zlabel='z', data_label='data', cmap=plt.get_cmap('jet'),
                 anim_z=True, z_fmt=r'%.3f', static_cbar=True):
        self.x_axis = x_axis
        self.data = data
        self.cmap = cmap
        self.xlabel = xlabel
        self.data_label = data_label
        self.static_cbar = static_cbar
        self.anim_z = anim_z
        self.z_fmt = z_fmt
        if self.anim_z:
            self.y_axis = y_axis
            self.z_axis = z_axis
            self.ylabel = ylabel
            self.zlabel = zlabel
        else:
            # Store z as y and visa versa so they are swapped in images
            self.y_axis = z_axis
            self.z_axis = y_axis
            self.ylabel = zlabel
            self.zlabel = ylabel
        self._prepare_figure_and_axes()

    def _prepare_figure_and_axes(self):
        self.fig, self.ax = plt.subplots()
        # Manually set fig resolution to ensure consistent output
        self.fig.set_size_inches([8., 5.5])
        self.fig.set_dpi(72.0)
        self.ax.set_xlim((min(self.x_axis), max(self.x_axis)))
        self.ax.set_ylim((min(self.y_axis), max(self.y_axis)))

    def animate(self):
        self._plot_first_animation_frame()
        return animation.FuncAnimation(self.fig, self._anim_fn,
                                       frames=len(self.z_axis))

    def _plot_first_animation_frame(self):
        # Prepare mesh
        self.x_mesh, self.y_mesh = np.meshgrid(self.x_axis, self.y_axis)
        # Make a contour plot with the correct x axis, y axis and
        # colorbar so that these aspects can be labelled
        contour_plot = plt.pcolormesh(self.x_mesh, self.y_mesh,
                                      self._get_data(0),  # Any z will do
                                      cmap=self.cmap)
        # Set colorbar limits - very important to ensure correctness
        # of the animation with a static colorbar
        self.clim = [np.min(self.data), np.max(self.data)]
        if self.static_cbar:
            contour_plot.set_clim(self.clim)
        # Set labels
        plt.colorbar(label=self.data_label)
        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)
        # Ensure sufficient space for xlabel
        plt.tight_layout()

    def _anim_fn(self, frame_index):
        # The cmap must be passed to pcolormesh each time to ensure
        # that each frame is coloured in
        frame = plt.pcolormesh(self.x_mesh, self.y_mesh,
                               self._get_data(frame_index), cmap=self.cmap)
        # Set colorbar limits - very important to ensure correctness
        # of the animation with a static colorbar - must be done for
        # each frame
        if self.static_cbar:
            frame.set_clim(self.clim)
        # Use the title to convey the aniation axis coordinate
        plt.title(self.zlabel + r' = ' + self.z_fmt % self.z_axis[frame_index])
        return frame

    def _get_data(self, i):
        # This is also needed to ensure the correct data is plotted
        return self.data[i, :, :] if self.anim_z else self.data[:, i, :]
