"""
Unit tests of test general

Copyright 2019 Stephen Biggs-Fox

This file is part of tests.

tests is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tests is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with tests.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import numpy as np
import numpy.testing as np_test
import sys
sys.path.insert(0, '../')
import general as gen


class TestGeneral(TestCase):

    def test_iterate_axes_can_add_one(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5 * 6).reshape(2, 3, 4, 5, 6)
        axes = np.array([0, 1, 2])
        function = self.add_one
        expected = array + 1
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_add_one_different_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5 * 6).reshape(2, 3, 4, 5, 6)
        axes = np.array([3, 1])
        function = self.add_one
        expected = array + 1
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_equal_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5 * 5).reshape(2, 3, 4, 5, 5)
        axes = np.array([0, 1, 2])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 1, 2, 4, 3])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_other_equal_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 3 * 6).reshape(2, 3, 4, 3, 6)
        axes = np.array([0, 2, 4])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 3, 2, 1, 4])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_other_equal_axes_reverse_order(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 3 * 6).reshape(2, 3, 4, 3, 6)
        axes = np.array([4, 2, 0])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 3, 2, 1, 4])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_unequal_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5 * 6).reshape(2, 3, 4, 5, 6)
        axes = np.array([0, 1, 2])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 1, 2, 4, 3])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_different_unequal_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5).reshape(2, 3, 4, 5)
        axes = np.array([0, 3])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 2, 1, 3])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def test_iterate_axes_can_transpose_another_different_unequal_axes(self):
        # Setup
        array = np.arange(2 * 3 * 4 * 5).reshape(2, 3, 4, 5)
        axes = np.array([0, 1])
        function = self.transpose
        expected = np.transpose(array, axes=[0, 1, 3, 2])
        # Exercise SUT
        actual = gen.iterate_axes(array, axes, function)
        # Verify
        np_test.assert_array_equal(actual, expected)

    def add_one(self, array):
        return array + 1

    def transpose(self, array):
        return np.transpose(array)
