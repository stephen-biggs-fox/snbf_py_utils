"""
Tests for my_stats_utils

Copyright 2017, 2018 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""


import unittest
import numpy as np
import numpy.testing as nptest

# add parent directory to python search path so that the modules being
# tested can be found for import
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(
    os.path.realpath(__file__)), os.pardir)))

# import the modules to be tested
import stats


class testMyStatsUtils(unittest.TestCase):

    def test_normalise_to_range(self):
        # Setup
        data = [-2., 0., 2.]
        # Specify expected result
        expected = [5., 7.5, 10.]
        # Exercise system under test
        actual = stats.normalise_to_range(data, 5., 10.)
        # Verify result
        nptest.assert_array_equal(actual, expected)

    def test_normalise_to_range_again(self):
        # Setup
        data = [1.5, 1.6, 1.7, 1.8, 1.9]
        # Specify expected result
        expected = [-10., -9., -8., -7., -6.]
        # Exercise system under test
        actual = stats.normalise_to_range(data, -10., -6.)
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_normalise_to_range_2d(self):
        # Setup
        data = np.array([
            [1.1, 1.2, 1.3],
            [2.1, 2.2, 2.3],
            [3.1, 3.2, 3.3],
        ])
        # Specify expected result
        expected = np.array([
            [11., 12., 13.],
            [21., 22., 23.],
            [31., 32., 33.],
        ])
        # Exercise system under test
        actual = stats.normalise_to_range(data, 11., 33.)
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_normalise_leaves_data_unchanged(self):
        # Setup
        data = [-2., 0., 2.]
        # Specify expected result
        expected = [-2., 0., 2.]
        # Exercise system under test
        stats.normalise_to_range(data, 5., 10.)
        # Verify result
        nptest.assert_array_equal(data, expected)
