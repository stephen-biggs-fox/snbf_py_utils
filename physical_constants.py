"""
Module containing values for common physical constants in SI to high precision

Copyright 2018 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np

# Reference:
# https://physics.nist.gov/cuu/Constants/index.html

amu = 1.66053906660e-27  # atomic mass unit in kg
m_p = 1.67262192369e-27  # proton mass in kg
m_n = 1.67492749804e-27  # neutron mass in kg
m_e = 9.1093837015e-31  # electron mass in kg
m_d = 3.3435837724e-27  # deuteron mass in kg
m_t = 5.0073567446e-27  # triton mass in kg
m_a = 6.6446573357e-27  # alpha mass in kg
e = 1.602176634e-19  # electron charge in C
k_B = 1.380649e-23  # Boltzmann constant in J/K
mu_0 = 1.25663706212e-6  # vacuum permeability (magnetic constant) in N/A^2
epsilon_0 = 8.8541878128e-12  # vacuum permittivity (electric constant) in F/m
h = 6.62607015e-34  # Planck constant in J.s
hbar = h / (2. * np.pi)  # reduced Planck constant J.s
c = 2.99792458e8  # speed of light in m/s
