"""
Generic class to represent a differential operator on a supplied x-grid.

Copyright 2016 David Dickinson

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
from scipy.sparse import lil_matrix, csr_matrix
from scipy.sparse.linalg import factorized, eigs as sparse_eigs
from scipy.linalg import eig as full_eig
from numpy import ndarray, zeros, append

class difOp():
    """Generic class to represent a differential operator of the form:
           a*d^2/dx^2 + b*d/dx + c
       on a supplied x-grid.
    """

    def __init__(self,xgrid,aCoef,bCoef,cCoef,construct=True,*args,**kwargs):
        self.__xgrid = xgrid
        self.__nx = len(xgrid)
        #Ensure coeffcients are arrays of the correct type and size
        if not isinstance(aCoef,ndarray):
            aCoef = zeros(len(self)) + aCoef
        if not isinstance(bCoef,ndarray):
            bCoef = zeros(len(self)) + bCoef
        if not isinstance(cCoef,ndarray):
            cCoef = zeros(len(self)) + cCoef

        self.__aCoef = aCoef
        self.__bCoef = bCoef
        self.__cCoef = cCoef
        self.__dx = xgrid[1]-xgrid[0]
        self.__constructed=False

        self.__lastFactorHash = None
        self.__solveRoutine = None

        if construct:
            self.__constructMatrix(self,*args,**kwargs)

    def __varName(self,nm):
        """
        Returns a string representing the name of internal objects.
        Mainly used in __getitem__.
        """
        return "_"+self.__class__.__name__+"__"+nm

    def __getitem__(self,key):
        """
        Access internal objects using getattr. This somewhat negates the point of
        making variables privateish (starting with __).
        """
        return getattr(self,self.__varName(key))

    def __constructMatrix(self,*args,**kwargs):
        """Constructs the actual matrix operator based on
        internal data."""
        if self.__constructed:
            warnings.warn("Warning: Overriding previously constructed matrix",UserWarning)

        dx = self.__dx
        dxSqInv  = 1.0/dx**2
        twoDxInv = 0.5/dx
        aCoef = self.__aCoef
        bCoef = self.__bCoef
        cCoef = self.__cCoef

        aFac = dxSqInv
        bFac = twoDxInv
        cFac = 1.0

        if 'dtype' in kwargs.keys():
            dt = kwargs['dtype']
        else:
            dt = 'd'
        mat = lil_matrix((self.__nx,self.__nx),dtype=dt)

        #Populate centre of operator
        for i in range(1,self.__nx-1):
            mat[i,i-1] =    aCoef[i]*aFac - bCoef[i]*bFac
            mat[i,i]   = -2*aCoef[i]*aFac                     + cCoef[i]*cFac
            mat[i,i+1] =    aCoef[i]*aFac + bCoef[i]*bFac

        #Now do the boundaries
        mat = self.__setBoundaries(matIn=mat,**kwargs)

        #Store as csr matrix
        self.__mat = mat.tocsr()
        self.__mat.eliminate_zeros()
        self.__constructed = True

    def __setBoundaries(self,matIn=None,enforceBC=False,periodic=False,**kwargs):
        #Unpack
        dx    = self.__dx
        dxSqInv  = 1.0/dx**2
        twoDxInv = 0.5/dx

        aCoef = self.__aCoef
        bCoef = self.__bCoef
        cCoef = self.__cCoef

        aFac = dxSqInv
        bFac = twoDxInv
        cFac = 1.0

        if matIn is None:
            mat = self.__mat
        else:
            mat = matIn

        #Convert to easier type
        mat = mat.tolil()

        if not enforceBC:#Second order FD approx
            if not periodic:#Second order FD approx
                #LHS
                mat[0,0]   =  2*aCoef[0]*aFac - 3*bCoef[0]*bFac + cCoef[0]*cFac
                mat[0,1]   = -5*aCoef[0]*aFac + 4*bCoef[0]*bFac
                mat[0,2]   =  4*aCoef[0]*aFac - 1*bCoef[0]*bFac
                mat[0,3]   = -1*aCoef[0]*aFac

                #RHS
                mat[-1,-1] =  2*aCoef[-1]*aFac + 3*bCoef[-1]*bFac + cCoef[-1]*cFac
                mat[-1,-2] = -5*aCoef[-1]*aFac - 4*bCoef[-1]*bFac
                mat[-1,-3] =  4*aCoef[-1]*aFac + 1*bCoef[-1]*bFac
                mat[-1,-4] = -1*aCoef[-1]*aFac
            else:
                #LHS
                mat[0,-1]  =    aCoef[0]*aFac - bCoef[0]*bFac
                mat[0, 0]  = -2*aCoef[0]*aFac                         + cCoef[0]*cFac
                mat[0, 1]  =    aCoef[0]*aFac + bCoef[0]*bFac

                #RHS
                mat[-1,-2]  =    aCoef[-1]*aFac - bCoef[-1]*bFac
                mat[-1,-1]  = -2*aCoef[-1]*aFac                         + cCoef[-1]*cFac
                mat[-1, 0]  =    aCoef[-1]*aFac + bCoef[-1]*bFac

        else: #Dirichlet(0.0) BC -- could add more
            mat[0,0] = 1.0 ; mat[0,1] = 0 ; mat[0,2] = 0 ; mat[0,3] = 0
            mat[-1,-1] = 1.0 ; mat[-1,-2] = 0 ; mat[-1,-3] = 0 ; mat[-1,-4] = 0

        mat = mat.tocsr()
        mat.eliminate_zeros()
        if matIn is None:
            self.__mat = mat
            return mat
        else:
            return mat

    def __setMat(self,mat):
        """Sets the operators matrix to the passed in matrix. Does minimal checking so be careful"""
        if self.__constructed:
            warnings.warn("Overriding previously constructed matrix",UserWarning)

        #Do some type checking
        intMat = mat
        if isinstance(intMat, difOp):
            intMat = difOp.__getMat()
        if isinstance(intMat, lil_matrix):
            intMat = intMat.tocsr()
        if not isinstance(intMat, csr_matrix):
            raise TypeError("Trying to set operator matrix with none matrix type",mat,intMat)

        #Set the matrix and flag as constructed
        self.__mat = intMat
        self.__constructed = True
        self.__resetFactorised()

    def __getMat(self):
        """Returns the operators matrix"""
        return self.__mat

    def __len__(self):
        """Returns an integer representing the size of a single dimension"""
        return len(self.__xgrid)

    def __call__(self,vector):
        """Applies the differential operator to the vector"""
        return self.__mat.dot(vector)

    def __add__(self,op):
        """Returns a new difop that represents the sum of the two operators."""
        if not isinstance(op, difOp):
            raise TypeError("Can only sum difOp with other difOp",self,op)

        #Work out the new coefficients
        aCoef = self.__aCoef + op.__aCoef
        bCoef = self.__bCoef + op.__bCoef
        cCoef = self.__cCoef + op.__cCoef

        #Create the new operator but don't construct
        newOp = difOp(self.__xgrid,aCoef,bCoef,cCoef,construct=False)
        #Now set the matrix from the two operators
        newOp.setMat(self.__getMat() + op.__getMat())

        #Return
        return newOp

    def __sub__(self,op):
        """Returns a new difop that represents the subtraction of the two operators."""
        if not isinstance(op, difOp):
            raise TypeError("Can only subtract difOp with other difOp",self,op)

        #Work out the new coefficients
        aCoef = self.__aCoef - op.__aCoef
        bCoef = self.__bCoef - op.__bCoef
        cCoef = self.__cCoef - op.__cCoef

        #Create the new operator but don't construct
        newOp = difOp(self.__xgrid,aCoef,bCoef,cCoef,construct=False)
        #Now set the matrix from the two operators
        newOp.setMat(self.__getMat() - op.__getMat())

        #Return
        return newOp

    def __str__(self):
        string = """difOp instance of a*d^2/dx^2 + b*d/dx + c for a={a},
        b={b} and c={c} on a grid of size {nx}""".format(a=self.__aCoef,b=self.__bCoef,
                                                         c=self.__cCoef,nx=len(self))
        return string

    def __getitem__(self,key):
        return self.__mat[key]

    def _extend(self,op):
        """Extends the operator to include the current operation and that of the passed op operator"""
        if self.__dx != op.__dx:
            warnings.warn("""Extending operator with an operator with different grid spacing.
            Future use of dx will likely be incorrect.""",UserWarning)

        #Trivial extends
        self.__xgrid = append(self.__xgrid,(op.__xgrid))
        self.__aCoef = append(self.__aCoef,(op.__aCoef))
        self.__bCoef = append(self.__bCoef,(op.__bCoef))
        self.__cCoef = append(self.__cCoef,(op.__cCoef))

        #Now merge the two matrices
        self.__mat = mergeMats([self.__mat,op.__mat])

    def _toPetsc(self):
        """Tries to convert the matrix to a PETSc aij matrix.
        Only suitable if the petsc4py module is installed."""

        try:
            from petsc4py import PETSc as pts
            pmat = pts.Mat().createAIJ(size=self.__mat.shape,
                                       csr=(self.__mat.indptr,self.__mat.indices,self.__mat.data))
        except ImportError:
            print("Error: Trouble importing petsc4py")
            pmat = None

        return pmat

    def _factor(self):
        """Factorises the matrix in preparation for a solve."""
        from time import time
        changed = False
        #If the matrix hash has changed better refactorise
        if self.__lastFactorHash is None:
            print("\t Matrix to be factorised for first time")
            t0=time()
            self.__solveRoutine = factorized(self.__mat.tocsc())
            t1=time()
            self.__lastFactorHash=hash(self.__mat.data.tostring())
            t2=time()
            changed = True
        elif not hash(self.__mat.data.tostring()) == self.__lastFactorHash:
            print("\t Matrix has changed so re-factorising")
            t0=time()
            self.__solveRoutine = factorized(self.__mat.tocsc())
            t1=time()
            self.__lastFactorHash=hash(self.__mat.data.tostring())
            t2=time()
            changed = True

        if changed: print("\t Updated the factorisiation in {t} s + {t2} s".format(t=t1-t0,t2=t2-t1))

    def solve(self,rhs,bcVal=None):
        """Solves the sparse matrix equation A.x=b for x, where this instance
        represents A, and the argument is b"""
        #Ensure we're factorised
        self._factor()

        #If we want to solve we should enforce the BC.
        if bcVal is not None:
            rhs = rhs.reshape([-1,self.__nx])
            rhs[:,0] = bcVal
            rhs[:,-1] = bcVal
            rhs = rhs.flatten()
        return self.__solveRoutine(rhs)

    def dot(self,vector):
        return self.__call__(vector)

    def getEigs(self,sparse=False,nEigs=None, which='LI'):
        """Get eigenvalues and eigenvectors using dense or sparse matrix
        computation as requested by caller (default: dense)"""
        if not sparse:
            # use eigs from scipy.linalg instead of scipy.sparse.linalg -
            # matrix is small enough that dense operations are probably OK and
            # sparse operations are unnecessary. If performance becomes an issue,
            # can use tridiagonal version to exploit periodic boundary conditions.
            eigvals, eigvecs = full_eig(self.__mat.toarray())
        else:
            if nEigs is None:
                nEigs = len(self)-2 #Can't get all eigenvals for some reason

            #Returns eigval, eigvect, LI for largest imaginary eigenvalue preferred
            eigvals, eigvecs = sparse_eigs(self.__mat,k=nEigs,which=which,tol=1.0e-5)
        return eigvals, eigvecs


    # For safe debugging, e.g. testing non-sparse eigenvalue computation
    def getMat(self):
        return self.__mat.copy()
