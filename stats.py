"""
Python module containing statistics utilities

Copyright 2016, 2017, 2018 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""

# Import math functions
from numpy import mean, std, sqrt, polyfit, diag, floor, log10, isscalar
from numpy import array, isinf
import numpy as np

from general import preallocate_based_on

# ---------------------------- Functions -----------------------------


def normalise_to_range(data, desired_min, desired_max):
    'Normalises data to a specified range'
    data -= np.min(data)
    return data / np.max(data) * (desired_max - desired_min) + desired_min


def compute_mean_and_std_err(sample):
    'Computes the mean of a sample and the standard error on the mean'
    the_mean = mean(sample)
    err = std(sample)/sqrt(len(sample))
    return round_val_to_err(the_mean, err)


def polyfit_with_errors(x, y, deg):
    """Compute a polynomial fit of degree deg including the estimated
    uncertainty in each of the coefficients"""
    coeffs, cov = polyfit(x, y, deg, cov=True)
    if isinf(cov).any():
        errors = compute_fit_errors_manually(x, y, deg)
    else:
        errors = sqrt(diag(cov))
    return round_val_to_err(coeffs, errors)


def compute_fit_errors_manually(x, y, deg):
    """Computes the fit errors as the standard error on the mean of
    each individual gradient segment and the intercept that it would
    generate"""
    if not deg == 1:
        'WARNING: Manual error calculation not implemented for deg /= 1.'
        gradient_error = 0.
        intercept_error = 0.
    else:
        gradients = [0.] * (len(x) - 1)
        intercepts = preallocate_based_on(gradients)
        for i in range(len(x) - 1):
            gradients[i] = (y[i+1] - y[i]) / (x[i+1] - x[i])
            intercepts[i] = y[i] - gradients[i] * x[i]
        _, gradient_error, _ = compute_mean_and_std_err(gradients)
        _, intercept_error, _ = compute_mean_and_std_err(intercepts)
    # Double the errors before returning to accound for poor quality
    # data
    return [2. * gradient_error, 2. * intercept_error]


def round_val_to_err(val, err):
    """Round value and error to same number of decimal places such
    that error is to 1 sig. fig."""
    if isscalar(val):
        vals, errs, dps = _scalar_round_val_to_err(val, err)
    else:
        vals = array(preallocate_based_on(val), dtype=float)
        errs = array(preallocate_based_on(val), dtype=float)
        dps = array(preallocate_based_on(val))
        for i in range(len(val)):
            vals[i], errs[i], dps[i] = _scalar_round_val_to_err(
                val[i], err[i])
    return vals, errs, dps


def _scalar_round_val_to_err(val, err):
    """Round value and error to same number of decimal places such
    that error is to 1 sig. fig."""
    err = _round_to_1sf(err)
    dps = _count_dps(err)
    val = round(val, dps)
    return val, err, dps


def _round_to_1sf(x):  # private
    'Round to 1 sig. fig.'
    try:
        with np.errstate(divide='raise'):
            out = abs(round(x, -int(floor(log10(abs(x))))))
    except FloatingPointError:
            out = 0.
    return out


def _count_dps(x):  # private
    'Count number of decimal places'
    dps = 0
    num_tries = 0
    try:
        with np.errstate(divide='raise'):
            while(log10(x) < 0.0 and num_tries < 16):
                dps += 1
                x *= 10
                num_tries += 1
    except FloatingPointError:
        dps = 1
    return dps

# ------------------------ End Functions -----------------------------
