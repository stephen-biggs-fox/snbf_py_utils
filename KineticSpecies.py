"""
Represents a kinetic plasma species

Copyright 2019 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np
import physical_constants as phys


class KineticSpecies:

    def __init__(self, temperature_eV, number_density_m3, mass_kg=None,
                 proton_number=None, name=None):
        """Initialises a new plasma kinetic species
        Inputs:
            - temperature_eV - species temperature in electronvolts
            - number_denisty_m3 - species density in number of particles per
            cubic meter
            - mass_kg - species mass in kilograms
            - proton_number - species atomic number (number of protons) - for
            electrons this should be minus 1 (but use name='e')
            - name - one of a variety of predefined species names to
            automatically populate the mass and charge variables ('e' =
            electrons, H = hydrogen ions, D = deuterium ions, T = tritium ions,
            He = helium ions)
        NB: Either the namemust be given or both mass and proton number must be
        given.
        """
        self.temperature_eV = temperature_eV
        self.number_density_m3 = number_density_m3
        masses_and_charges = {
            'e': [phys.m_e, -1],
            'H': [phys.m_p, 1],
            'D': [phys.m_d, 1],
            'T': [phys.m_t, 1],
            'He': [phys.m_a, 2]
        }
        if name is not None:
            try:
                self.mass_kg = masses_and_charges[name][0]
                self.proton_number = masses_and_charges[name][1]
            except(KeyError):
                raise(KeyError("Unrecognised species name"))
        else:
            self.mass_kg = mass_kg
            self.proton_number = proton_number
        if self.mass_kg is None or self.proton_number is None:
            raise(ValueError("Must specify species name or mass and proton" +
                             "number"))

    def compute_debye_length(self):
        "Computes the Debye length of this species"
        # https://farside.ph.utexas.edu/teaching/plasma/lectures1/node6.html#e1.8,
        # eq. (8)
        return np.sqrt(phys.epsilon_0 * phys.e * self.temperature_eV /
                       (self.number_density_m3 *
                        (self.proton_number * phys.e)**2))

    def compute_plasma_parameter(self):
        """Compute the plasma parameter (a.k.a. Coulomb logarithm, Lambda_c)
        NB: This returns the un-logged version, i.e. Lambda_c, not
        ln[Lambda_c]"""
        # https://farside.ph.utexas.edu/teaching/plasma/lectures1/node8.html,
        # eq. (19)
        return (4. * np.pi * self.number_density_m3 *
                self.compute_debye_length()**3)

    def compute_plasma_frequency(self):
        "Computes the plasma frequency of this species"
        # https://farside.ph.utexas.edu/teaching/plasma/lectures1/node6.html,
        # eq. (5)
        return np.sqrt(self.number_density_m3 *
                       (self.proton_number * phys.e)**2 /
                       (phys.epsilon_0 * self.mass_kg))

    def compute_collision_time(self):
        "Compute the collision time for this species"
        # Wesson (2011), eq. (2.15.1)
        return (12. * np.sqrt(2.) * np.pi**(3. / 2.) * phys.epsilon_0**2 *
                self.mass_kg**(1. / 2.) *
                (phys.e * self.temperature_eV)**(3. / 2.) /
                (np.log(self.compute_plasma_parameter()) *
                 (self.proton_number * phys.e)**4 *
                 self.number_density_m3))

    def compute_collision_frequency(self):
        "Computes the collision frequency of this species"
        return 1. / self.compute_collision_time()

    def compute_thermal_velocity(self):
        "Computes the thermal velocity of this species"
        return np.sqrt(2. * phys.e * self.temperature_eV / self.mass_kg)

    def normalise_collision_frequency(self, thermal_velocity_ms1,
                                      reference_length_m):
        """Returns the collision frequency normalised to the given length and
        velocity"""
        collision_frequency_s1 = self.compute_collision_frequency()
        return (collision_frequency_s1 * reference_length_m /
                thermal_velocity_ms1)
