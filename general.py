"""
Python module containing general utilities

Copyright 2016, 2018 Stephen Biggs-Fox

This file is part of snbf_py_utils.

snbf_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf_py_utils.  If not, see http://www.gnu.org/licenses/.
"""

import glob
import numpy as np
import pickle as pkl
from scipy import interpolate as sp_interp


def preallocate_based_on(array):
    'Preallocate an array of the same size as the input array'
    return [0] * len(array)  # Same size, zero valued


def find_files_matching_pattern(pattern):
    'Finds all files in the current folder that match the specified pattern'
    return glob.glob(pattern)


def get_sign(value):
    'Gets the sign of value, e.g. for use in strings'
    sign = '+'
    if value < 0.0:
        sign = '-'
    return sign


def extract_subarrays_via_logical_index(arrays, logical_index):
    """Extracts from each of the input arrays a subarray that contains the
    elements specified by the logical index"""
    for i, array in enumerate(arrays):
        arrays[i] = extract_subarray_via_logical_index(array, logical_index)
    return tuple(arrays)


def extract_subarray_via_logical_index(array, logical_index):
    """Extracts from the input array a subarray that contains the elements
    specified by the logical index"""
    return [x for i, x in enumerate(array) if logical_index[i]]


def remove_inf_and_nan(array):
    """Removes any inf and nan elements from an array. The logical_index is also
    returned in case any other arrays need to be clipped accordingly."""
    logical_index = np.logical_not(np.isinf(array) + np.isnan(array))
    subarray = extract_subarray_via_logical_index(array, logical_index)
    return subarray, logical_index


def save_obj(obj, filename):
    """obj = object to be saved
    filename = string path and filename of where to save to"""
    with open(filename, 'wb') as f:
        pkl.dump(obj, f, pkl.HIGHEST_PROTOCOL)


def load_obj(filename):
    'filename = string path and filename of where to load from'
    with open(filename, 'rb') as f:
        return pkl.load(f)


def print_keys(my_dict):
    'Print keys with shapes / lengths / values from supplied dict'
    keys = my_dict.keys()
    keys.sort()
    for k in keys:
        try:
            shape = my_dict[k].shape
            print(k + ".shape = " + str(shape))
        except AttributeError:
            try:
                my_len = len(my_dict[k])
                print("len(" + k + ") = " + str(my_len))
            except TypeError:
                print(k + " = " + str(my_dict[k]))


def iterate_axes(array, axes, function, output_array=None):
    """Iterates over the specified axes of the array
    Inputs:
        - array - numpy array - the array to be iterated
        - axes - numpy array - the axes to iterate
        - function - python function - the function to perform upon the
            subarrays that are not iterated over
    Output:
        None
    NB: The function that is executed on each of the subarrays that is not
    iterated over should return a numpy array that is of the same shape every
    time for a given input shape. Scalars should be in a numpy array of shape
    (1, ). The array gets stored within a higher dimensional array where the
    other dimensions are of the same length as the axes that have been iterated
    over."""
    # Move axis to the end
    array_with_moved_axis = np.moveaxis(array, axes[0], -1)
    # Iterate over the axis that we have moved
    for i in range(array_with_moved_axis.shape[-1]):
        # Extract subarray
        subarray = array_with_moved_axis[..., i]
        # If there are more axes, recurse
        if len(axes) > 1:
            # Remove used axis from list using `for x in axes[1:]`, find axes
            # that are after the removed one using `if x > axes[0]` and move
            # them back by one using `x - 1` to account for removed axis while
            # leaving the others unchanged using `else x`.
            new_axes = np.array([x - 1 if x > axes[0] else x
                                 for x in axes[1:]])
            # Recurse
            result = iterate_axes(subarray, new_axes, function)
            # Setup output_array
            if output_array is None:
                shape = list(result.shape)
                shape.append(array_with_moved_axis.shape[-1])
                output_array = np.ndarray(shape, dtype=result.dtype)
            # Store result
            output_array[..., i] = result
        else:
            # else, call the function
            result = function(subarray)
            if output_array is None:
                shape = list(result.shape)
                shape.append(array_with_moved_axis.shape[-1])
                output_array = np.ndarray(shape, dtype=result.dtype)
            output_array[..., i] = result
    output_array = np.moveaxis(output_array, -1, axes[0])
    return output_array


def get_extrema(x, y):
    # k=4 so derivative is k=3 so we can find roots of derivative
    spline = sp_interp.InterpolatedUnivariateSpline(x, y, k=4)
    return spline(spline.derivative().roots())
