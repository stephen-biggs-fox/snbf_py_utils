# snbf\_py\_utils

A collection of utility functions by Stephen Biggs-Fox for use in python2

Copyright 2016, 2017, 2018 Stephen Biggs-Fox

This file is part of snbf\_py\_utils.

snbf\_py\_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

snbf\_py\_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with snbf\_py\_utils.  If not, see http://www.gnu.org/licenses/.

# Dependencies

- python 2.x
- numpy
- matplotlib

NB: python3 currently not explicitly supported but some functions might work in python3 anyway

# Running Tests

From the root directory of the project:

`python -m unittest discover`

# Using Functions

Either:

1. Add the root directory of the project to your PYTHONPATH environment variable or programatically
   using `import sys; sys.path.insert(0, dir_name)` where `dir_name` is the root directory of the
project. Then, for example, `import general as gen; gen.print_keys(your_dict)`. Or;

2. Add the directory above the root directory of the project to your PYTHONPATH environment variable
   or programatically using `import sys; sys.path.insert(0, dir_name)` where `dir_name` is the
directory above the root directory of the project. Then, for example, `import snbf_py_utils.general
as gen; gen.print_keys(your_dict)`.

# Getting Help

To see the functions available and get help using them, run (for example) `import general as gen;
help(gen)`

# Contributing

This is primarily a project for my own use. As such, I am not expecting substantial contributions
from others. However, if you find any bugs or have any suggestions for improvements, please contact
me by email. My email address is available in the git log.
